/* 
 * Tomaxus JavaScript collection ...
 * 
 */
$(document).ready(function() {
        
    // Hide the content of the element with class help_body
    $(".help_body").hide();
  
    // Toggle help content
    $(".help_head").click(function()
    {
        $(this).next(".help_body").slideToggle(100);
    });
    
    // DatePicker: If the id ends with "datepicker" make it a jQuery datepicker
    $(function() {
        $('input[id$="datepicker"]').datepicker({
            dateFormat: 'yy-mm-dd'
        } ).val();
    });


});