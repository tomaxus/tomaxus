/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import com.icesoft.faces.component.selectinputtext.TextChangeEvent;
import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Daniel
 */
@SessionScoped
@Named
public class UserSearchUtilityBean implements Serializable {

    private String selectedText = "";
    private List<TUser> availableUsers;
    private List<SelectItem> availableUserItem;
    @Inject
    private UserRegistryBean reg;
    @Inject
    private ProfileBB profile;
    TUser gubbe1;
    TUser gubbe2;
    TUser gubbe3;
    TUser gubbe4;

    public UserSearchUtilityBean() {
        availableUsers = new ArrayList<>();
        availableUserItem = new ArrayList<>();
        gubbe1 = new TUser("Springarn", "Johan", "Karlsson");
        gubbe2 = new TUser("Sven", "Gunnar", "Gunnarson");
        gubbe3 = new TUser("Clara", "Tiara", "Gunnarson");
        gubbe4 = new TUser("Bertil", "Fiskarn", "Jonsson");
        availableUsers.add(gubbe1);
        availableUsers.add(gubbe2);
        availableUsers.add(gubbe3);
        availableUsers.add(gubbe4);

    }

    public String getSelectedText() {

        return selectedText;
    }

    public void textChanged(TextChangeEvent event) {

        selectedText = event.getNewValue().toString();
        filterUsers(selectedText);
        getNames();

    }

    public void submitText(ActionEvent event) {

        if (!reg.checkUserName(selectedText)) {

            try {
                Logger.getAnonymousLogger().log(Level.INFO, selectedText);
                profile.setUser((reg.getUser(selectedText)).getId());
                FacesContext.getCurrentInstance().getExternalContext().redirect("profile.xhtml");
            } catch (IOException ex) {
                ex.printStackTrace();
                
            }
        }
    }

    public List<TUser> getAvailableUsers() {
        return availableUsers;
    }

    public void setAvailableUsers(List<TUser> availalbeUsers) {
        this.availableUsers = availalbeUsers;
    }

    public void setAvailableUserItem(List<SelectItem> items) {
        availableUserItem = items;
    }

    public List<SelectItem> getAvailableUserItem() {
        return availableUserItem;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    private void filterUsers(String s) {

        availableUsers.clear();
        List<TUser> users = reg.getAll();
        for (TUser u : users) {
            if (u.getUsername().toLowerCase().startsWith(s.toLowerCase())) {
                availableUsers.add(u);
            }
        }
        if (availableUsers.isEmpty()) {

            availableUsers.add(new TUser("", "", ""));
        }
    }

    private void getNames() {

        availableUserItem.clear();

        for (TUser u : availableUsers) {
            availableUserItem.add(new SelectItem(u, u.getUsername()));
        }
    }

    public List<TUser> getUsers() {
        return reg.getAll();
    }
}