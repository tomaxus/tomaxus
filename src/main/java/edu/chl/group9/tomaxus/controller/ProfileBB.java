package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.controller.security.UserLoginBB;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * Bean for showing a specific TUser in profile.xhtml
 * 
 * Specify UserID before getters.
 * 
 * @author Erik
 */
@Named("profile")
@SessionScoped
public class ProfileBB implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    UserRegistryBean userReg;
    
    @Inject
    UserLoginBB userLogin;
    
    @Inject
    EventRegistryBean eventReg;
    @Inject
    private Conversation conv;
    
    private TUser selectedUser;
    
    public void setUser(long id) {
        if (conv.isTransient()) {
            conv.begin();
        } 
        selectedUser = userReg.find(id);
    }
    
    public void endConv(){
        if (conv != null) {
            if (!conv.isTransient()) {
                conv.end();
            }
        }
    }
        
    @PreDestroy
    public void destroy() {
        if (conv != null) {
            if (!conv.isTransient()) {
                conv.end();
            }
        }
    }
    
    public long getId() {
        return selectedUser.getId();
    }
    
    public boolean getHasCustomImage() {
        return getImageId() != 0;
    }
    
    public long getImageId() {
        return selectedUser.getProfileImageId();
    }
            
    public String getUsername(){
            return selectedUser.getUsername();
    }  
    
    
    public Location getLocation() {
        return selectedUser.getLoc();
    }
    
    public String getFname(){
            return selectedUser.getFname(); 
    }   
    
    public String getLname(){
            return selectedUser.getLname();
    }
    
       
    public String getGender(){
            return selectedUser.getGender(); 
    }   
    
    public int getAge(){
            return selectedUser.getAge();
    }
    
    public List<Event> getEvents(){
        return eventReg.getEventsFromUser(selectedUser.getId());
    }    
    
    public List<TUser> getFriends() {
        return selectedUser.getFriends();
    }
          
    public boolean getIsThisMe() {
        return selectedUser.equals(userLogin.getUser());
    }

    public void removeAsFriend() {
        TUser me = userLogin.getUser();
        me.removeFriend(selectedUser);
        userReg.updateUser(me);
    }  

    public void addAsFriend() {
        TUser me = userLogin.getUser();
        me.addFriend(selectedUser);
        userReg.updateUser(me);
    }           
    public boolean getNotIsFriendsWith(){
        if(!getIsThisMe()) {
            return !getIsFriendsWith();
        } else {
            return false;
        }
    }
    public boolean getIsFriendsWith(){
        if(!getIsThisMe()) {
            TUser me = userLogin.getUser();
            return me.isFriendsWith(selectedUser);
        } else {
            return false;
        }
    }
}
