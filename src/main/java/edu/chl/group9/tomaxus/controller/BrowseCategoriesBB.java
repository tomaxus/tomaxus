/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.CategoryRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.model.Category;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Bean for browsecategories.
 *
 * @author Erik
 */
@Named("Browse")
@RequestScoped
public class BrowseCategoriesBB {
    @Inject
    private EventRegistryBean eventReg;
    
    @Inject
    private CategoryRegistryBean categoryReg;
    
    public List<Category> getAllCategories() {
        return categoryReg.getAll();
    }
    
}
