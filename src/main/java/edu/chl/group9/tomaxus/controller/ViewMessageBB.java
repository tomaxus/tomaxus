package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.MessageRegistryBean;
import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * Bean for showing a specific TUser in profile.xhtml
 * 
 * Specify UserID before getters.
 * 
 * @author Erik
 */
@Named("viewmessage")
@ConversationScoped
public class ViewMessageBB implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    UserRegistryBean userReg;
    
    private PersonalMessage m;
    
    private String title;
    private String content;
    
    @Inject
    MessageRegistryBean messageReg;
    
    @Inject
    private Conversation conv;
    
    public void setMessage(long id) {
        if (conv.isTransient()) {
            conv.begin();
        } 
        m = messageReg.find(id);
        title = m.getTitle();
        content = m.getMessageContent();
    }
    
    public void endConv(){
        if (conv != null) {
            if (!conv.isTransient()) {
                conv.end();
            }
        }
    }
        
    @PreDestroy
    public void destroy() {
        if (conv != null) {
            if (!conv.isTransient()) {
                conv.end();
            }
        }
    }
    
    public PersonalMessage getSelectedMessage() {
        m.setIsRead(true);
        messageReg.updateMessage(m);
        return m;
    }
    
    public void deleteMessage() {
        messageReg.deleteMessage(m.getId());
    }
}
