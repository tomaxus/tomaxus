/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.CategoryRegistryBean;
import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * Wrapping bean class for Category.
 * 
 * @author Johanna
 */
@Named("newcategory")
@SessionScoped
public class NewCategoryBB implements Serializable {
    
    @Inject 
    CategoryRegistryBean categoryReg;
        
    private String description;
    private String title;
    
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
       this.description = description;
    }
    
    public void setTitle(String title) {
       this.title = title;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void add() {
         categoryReg.addCategory(new Category(title, description, new ArrayList<Event>()));
    }

}
