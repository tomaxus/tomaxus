/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.CategoryRegistryBean;
import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *  Backingbean for findevent.xhtml
 * 
 *  List all events of a specific category
 */
@SessionScoped  
@Named("findEvents")
public class FindEventBB implements Serializable {
    
    @Inject
    private CategoryRegistryBean catbean;
    
    private Category category;
    
    public void setCategory(Category category) {
        this.category = category;
    }
    
    public List<Event> getEvents() {
        return catbean.getEventsFromCategory(category.getId());
    }
    
    public String getCategoryTitle() {
        return category.getName();
    }
    
}
