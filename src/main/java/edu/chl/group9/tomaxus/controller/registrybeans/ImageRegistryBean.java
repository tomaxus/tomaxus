/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.Image;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;

/**
 *
 */
@SessionScoped
public class ImageRegistryBean implements Serializable {
    
    public void addImage(Image i) {
        Tomaxus.INSTANCE.getImageRegistry().add(i);
    }
    
    public Image find(long id) {
        return Tomaxus.INSTANCE.getImageRegistry().find(id);
    }
    
    public Image update(Image i) {
        return Tomaxus.INSTANCE.getImageRegistry().update(i);
    }
    
    public void remove(long imageId) {
        Tomaxus.INSTANCE.getImageRegistry().remove(imageId);
    }
}
