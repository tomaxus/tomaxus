/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Tomaxus;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
/**
 *
 * Java bean for handling Category-class.
 *
 * @author Johanna
 *
 */
@Named("categoryRegistry")
@SessionScoped
public class CategoryRegistryBean implements Serializable {

    public List<Category> getAll() {
        return Tomaxus.INSTANCE.getCategoryRegistry().getAll();
    }

    public void addCategory(Category c) {
        Tomaxus.INSTANCE.getCategoryRegistry().add(c);
    }

    public Category getCategory(long id) {
        return Tomaxus.INSTANCE.getCategoryRegistry().find(id);
    }

    public Category getCategory(String cat) {
        return Tomaxus.INSTANCE.getCategoryRegistry().getCategory(cat);
    }
    
    public void updateCategory(Category c) {
        Tomaxus.INSTANCE.getCategoryRegistry().update(c);
    }

    public void removeCategory(long id) {
        Tomaxus.INSTANCE.getCategoryRegistry().remove(id);
    }

    //public List<Category> searchCategory(String s) {
    //    return Tomaxus.INSTANCE.getCategoryRegistry().search(s);
    //}
    public List<Event> getEventsFromCategory(long categoryId) {
        return Tomaxus.INSTANCE.getCategoryRegistry().getEventsFromCategory(categoryId);
    }
}
