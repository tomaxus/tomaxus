package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.BlogRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.controller.security.UserLoginBB;
import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * Backing bean for friends.xhtml
 *
 * @author Johanna
 */
@Named("friends")
@RequestScoped
public class FriendsBB implements Serializable {

    public enum EventType {

        CREATE_EVENT,
        BLOG_EVENT,
        PARTICIPATE_EVENT
    }

    public static class FlowEvent implements Comparable<FlowEvent> {

        private String creator;
        private String title;
        private String eventTitle;
        private String description;
        private Date dateCreated;
        private EventType eventType;
        private long eid;
        
        public FlowEvent(BlogEvent be) {
            creator = be.getAuthor().getUsername();
            title = be.getTitle();
            eventTitle = be.getEvent().getTitle();
            description = be.getBlogContent();
            dateCreated = be.getTimeCreated();
            eid = be.getEvent().getId();
            eventType = EventType.BLOG_EVENT;
        }

        public FlowEvent(Event e) {
            creator = e.getOwner().getUsername();
            title = eventTitle = e.getTitle();
            description = e.getDescription();
            dateCreated = e.getTimeCreated();
            eid = e.getId();
            eventType = EventType.CREATE_EVENT;
        }

        public String getCreator() {
            return creator;
        }

        public String getTitle() {
            return title;
        }
        
        public String getEventTitle() {
            return eventTitle;
        }
        public String getDescription() {
            return description;
        }

        public Date getDateCreated() {
            return dateCreated;
        }

        public EventType getEventType() {
            return eventType;
        }
        
        public long getEid() {
            return eid;
        }
        
        public String getText() {
            
            if (eventType == EventType.CREATE_EVENT) {
                return "The event " + title + " was created by " + creator + " on " + dateCreated;  
            } 
            else if (eventType == EventType.BLOG_EVENT) {
                return creator + " posted on the event " + eventTitle + " on " + dateCreated;  
            }
            else {
                return "Unimplemented event type";
            }
        }
        
        @Override
        public int compareTo(FlowEvent other) {
            if (dateCreated.compareTo(other.dateCreated) < 0) {
                return -1;
            }
            else if (dateCreated.compareTo(other.dateCreated) > 0) {
                return 1; 
            }
            else { /* If creation date is equal, sort on title */
                return title.compareTo(other.title);
            }
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null) {
                return false;
            }
            if (obj.getClass().equals(this.getClass())) {
                try {
                    FlowEvent other = (FlowEvent) obj;
                    return other.hashCode() == this.hashCode();
                } catch (Exception e) {
                    return false;
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            return title.hashCode() + description.hashCode() + dateCreated.hashCode();
        }
    }
    @Inject
    UserLoginBB userLogin;
    @Inject
    EventRegistryBean eventReg;
    @Inject
    BlogRegistryBean blogReg;

    /* Return all my friends */
    public List<TUser> getList() {
        return userLogin.getUser().getFriends();
    }

    /* Generate a flow ...  */
    public SortedSet<FlowEvent> getFlow() {
        SortedSet<FlowEvent> flow = new TreeSet<>();

        /* Get all events I participate in. */
        List<Event> allEvents = eventReg.getEventsFromUser(userLogin.getId());

        List<TUser> friends = userLogin.getUser().getFriends();

        /* Get all events my friends participate in. */
        for (TUser friend : friends) {
            allEvents.addAll(eventReg.getEventsFromUser(friend.getId()));
        }

        /* List with blog events for the events I or my friends participate in. */
        List<BlogEvent> blogs = new ArrayList<>();

        for (Event e : allEvents) {
            flow.add(new FlowEvent(e));
            blogs.addAll(blogReg.getBlogEvents(e.getId()));

        }

        for (BlogEvent b : blogs) {
            flow.add(new FlowEvent(b));
        }

        return flow;
    }
}
