package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.MessageRegistryBean;
import edu.chl.group9.tomaxus.controller.security.UserLoginBB;
import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * Bean for showing a specific TUser in profile.xhtml
 * 
 * Specify UserID before getters.
 * 
 * @author Erik
 */
@Named("inboxbean")
@RequestScoped
public class InboxBB implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    UserRegistryBean userReg;
    
    @Inject
    MessageRegistryBean messageReg;
    
    @Inject
    private Conversation conv;
    
    @Inject
    private UserLoginBB login;
    
    
    public List<PersonalMessage> getInboxMessages() {
        return messageReg.getInboxForUser(login.getUser());
    }
    
    public List<PersonalMessage> getSentMessages() { 
        return messageReg.getSentForUser(login.getUser());
    }
    
    public boolean getHasInboxMessages() {
        return !getInboxMessages().isEmpty();
    }
    
    public boolean getHasSentMessages() {
        return !getSentMessages().isEmpty();
    }
}
