package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author peter
 */
@SessionScoped
public class UserRegistryBean implements Serializable {

    public List<TUser> getAll() {
        return Tomaxus.INSTANCE.getUserRegistry().getAll();
    }
    
    public void addUser(TUser u) {
        Tomaxus.INSTANCE.getUserRegistry().add(u);
    }
    
    public void deleteUserById(long id) {
        Tomaxus.INSTANCE.getUserRegistry().remove(id);
    }
    
    public void deleteUser(TUser u) {
        Tomaxus.INSTANCE.getUserRegistry().remove(u);
    }
    
    public TUser updateUser(TUser u) {
        return Tomaxus.INSTANCE.getUserRegistry().update(u);
    }
    public TUser find(long id) {
        return Tomaxus.INSTANCE.getUserRegistry().find(id);
    }
    public TUser getUser(String username) {
        return Tomaxus.INSTANCE.getUserRegistry().getUser(username);
    }
    public boolean validateUser(String name, String password) {
        return Tomaxus.INSTANCE.getUserRegistry().validateUser(name, password);
    }
    public boolean checkUserName(String name) {
        return Tomaxus.INSTANCE.getUserRegistry().checkUserName(name);
    }
}

