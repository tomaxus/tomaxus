/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
/**
 * 
 * Java bean for handling Event-class.
 *
 * @author Erik
 */
@Named("eventRegistry")
@SessionScoped
public class EventRegistryBean implements Serializable {
    
    public List<Event> getAll(){
        return Tomaxus.INSTANCE.getEventRegistry().getAll();
    }
    
    public void addEvent(Event ev) {
        Tomaxus.INSTANCE.getEventRegistry().add(ev);
    }
    
    public Event getEvent(long id){
        return Tomaxus.INSTANCE.getEventRegistry().find(id);
    }
    
    public Event updateEvent(Event e){
        return Tomaxus.INSTANCE.getEventRegistry().update(e);
    }
    
    public void removeEvent(long id) {
        Tomaxus.INSTANCE.getEventRegistry().remove(id);
    }
    
    public List<Event> searchEvent(String s) {
        return Tomaxus.INSTANCE.getEventRegistry().search(s);
    }
    
    public List<TUser> getUsersFromEvent(long eventId) {
        return Tomaxus.INSTANCE.getEventRegistry().getUsersFromEvent(eventId);
    }
    
    public List<Event> getEventsFromUser(long userId) {
        return Tomaxus.INSTANCE.getEventRegistry().getEventsFromUser(userId);
    }
}
