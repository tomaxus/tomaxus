package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.ImageRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.BlogRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.EventTimeRange;
import edu.chl.group9.tomaxus.model.Image;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;

/**
 *
 * Backingbean for event.xhtml
 * 
 * Details of a single event
 * 
 * @author peter
 */
@SessionScoped
@Named("showEvent")
public class EventBB implements Serializable {

    @Inject
    private EventRegistryBean eventbean;
    @Inject
    private UserRegistryBean userbean;
    @Inject
    private BlogRegistryBean blogbean;
    @Inject
    private ImageRegistryBean imagebean;
    
    private Event e;
    
    private TUser loggedInUser;
    
    private String eventTitle;
    private String eventDesc;
    private boolean eventPrivate;
    private Location eventLocation;
    private Date eventCreated;
    private long eventImageId;
    private boolean eventAccomplished;
    
    private String blogTitle;
    private String blogContent;
    private boolean blogIsPrivate;
    
    private Date startDate;
    private Date endDate;
    private String startDateString;
    private String endDateString;
    

    
    public void setEvent(long eid) {
        
        this.e = eventbean.getEvent(eid);
        
        this.eventTitle = e.getTitle();
        this.eventDesc = e.getDescription();
        this.eventPrivate = e.getIsPrivate();
        this.eventLocation = e.getLocation();
        this.eventCreated = e.getTimeCreated();
        this.startDate = e.getEventTimeRange().getStartTime();
        this.endDate = e.getEventTimeRange().getEndTime();
        
        loggedInUser = userbean.getUser((String)FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("ontherun"));
    }
    
    // Changing event variables
    public void save() {
        e.setTitle(eventTitle);
        e.setDescription(eventDesc);
        e.setLocation(eventLocation);
        e.setIsPrivate(eventPrivate);
        e.setIsAccomplished(eventAccomplished);
        
        //Cred to Johanna
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        Date startEventTime = null;
        Date endEventTime = null;
        try {
            startEventTime = sdf.parse(startDateString);
            endEventTime = sdf.parse(endDateString);
        } catch (ParseException ex) {
            Logger.getLogger(EventBB.class.getName()).log(Level.SEVERE, null, ex);
        } 
        e.setEventTimeRange(new EventTimeRange(startEventTime, endEventTime));
        
        e = eventbean.updateEvent(e);
    }
    
    public List<TUser> getUsers() {
        return eventbean.getUsersFromEvent(e.getId());
    }
    
    public List<BlogEvent> getBlogEvent() {
        return blogbean.getBlogEvents(e.getId());
    }
    
    public void addBlogEntry() {
        blogbean.addBlogEvent(new BlogEvent(blogTitle, blogContent, blogIsPrivate, e,loggedInUser));
        blogContent = null;
        blogTitle = null;
    }
    
    public void addParticipant() {
        e.addParticipant(loggedInUser);
        e = eventbean.updateEvent(e);
    }
    
    public void removeParticipant() {
        e.removeParticipant(loggedInUser);
        e = eventbean.updateEvent(e);
        
        //Remove event and blogentries if no participants
        if (e.getParticipants().isEmpty()) {
            List<BlogEvent> b = blogbean.getBlogEvents(e.getId());
            for (BlogEvent be: b) {
                blogbean.deleteBlogEvent(be);
            }
            eventbean.removeEvent(e.getId());
        }
    }
    
    public String isPrivate() {
        if (e.getIsPrivate()) {
            return "This is a private event.";
        } else {
            return "This is a public event.";
        }
    }

    public boolean loggedInParticipant() {
        return getUsers().contains(loggedInUser);
    }
    
    //File upload listener
    public void listener(FileEntryEvent event) {
        FileEntry fileEntry = (FileEntry) event.getSource();
        FileEntryResults results = fileEntry.getResults();
        for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
            if (fileInfo.isSaved()) {
                try {
                    String filetype = fileInfo.getContentType();
                    byte[] b = Files.readAllBytes(fileInfo.getFile().toPath());
                    
                    Image i = new Image(filetype, b);
                    imagebean.addImage(i);
                    //get id
                    i = imagebean.update(i);
                    
                    //remove old profileImage
                    if(e.getImageId() > 0) {
                        imagebean.remove(e.getImageId());
                    }
                    
                    e.setImageId(i.getId());
                    e = eventbean.updateEvent(e);
                
                } catch (IOException ex) {
                    Logger.getLogger(SettingsBB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    //Setters and getters    
    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public boolean isEventAccomplished() {
        return eventAccomplished;
    }

    public void setEventAccomplished(boolean eventAccomplished) {
        this.eventAccomplished = eventAccomplished;
    }

     public void setStartDate(Date d){
        EventTimeRange etr = e.getEventTimeRange();
        etr.setStartTime(d);
        this.e.setEventTimeRange(etr);
    }
    
    public Date getStartDate(){
        return e.getEventTimeRange().getStartTime();
    }
    
    public Date getEndDate() {
        return e.getEventTimeRange().getEndTime();
    }
    
    public void setEndDate(Date d) {
        EventTimeRange etr = e.getEventTimeRange();
        etr.setEndTime(d);
        this.e.setEventTimeRange(etr);
    }
    
    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public Location getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(Location eventLocation) {
        this.eventLocation = eventLocation;
    }

    public Date getEventCreated() {
        return eventCreated;
    }

    public void setEventCreated(Date eventCreated) {
        this.eventCreated = eventCreated;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public boolean isBlogIsPrivate() {
        return blogIsPrivate;
    }

    public void setBlogIsPrivate(boolean blogIsPrivate) {
        this.blogIsPrivate = blogIsPrivate;
    }

    public boolean isEventPrivate() {
        return eventPrivate;
    }

    public void setEventPrivate(boolean eventPrivate) {
        this.eventPrivate = eventPrivate;
    }

    public long getEventImageId() {
        return e.getImageId();
    }

    public void setEventImageId(long eventImageId) {
        e.setImageId(eventImageId);
        e = eventbean.updateEvent(e);
    }

    public String getStartDateString() {
        return startDateString;
    }

    public void setStartDateString(String startDateString) {
        this.startDateString = startDateString;
    }

    public String getEndDateString() {
        return endDateString;
    }

    public void setEndDateString(String endDateString) {
        this.endDateString = endDateString;
    }
}
