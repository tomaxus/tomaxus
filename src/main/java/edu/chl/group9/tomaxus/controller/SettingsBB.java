/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.ImageRegistryBean;
import edu.chl.group9.tomaxus.model.Image;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;

/**
 * Backingbean for settings.xhtml
 *
 * @author peter
 */
@RequestScoped
@Named("settings")
public class SettingsBB implements Serializable {

    @Inject
    private UserRegistryBean userbean;
    @Inject
    private ImageRegistryBean imagebean;
    
    private TUser currentUser = new TUser();
    private String username;
    private String fname;
    private String lname;
    private String password;
    private Location loc;
    
    private long profileImageId;

    public SettingsBB() {
        //TODO fix hardcoded variable
        currentUser = (TUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userid");
        
        username = currentUser.getUsername();
        fname = currentUser.getFname();
        lname = currentUser.getLname();
        password = currentUser.getPassword();
        loc = currentUser.getLoc();
        profileImageId = currentUser.getProfileImageId();
    }

    public void save() {
        currentUser.setFname(fname);
        currentUser.setLname(lname);
        currentUser.setLoc(loc);
        currentUser.setPassword(password);
        currentUser = userbean.updateUser(currentUser);
    } 
    
    //File upload listener
    public void listener(FileEntryEvent event) {
        FileEntry fileEntry = (FileEntry) event.getSource();
        FileEntryResults results = fileEntry.getResults();
        for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
            if (fileInfo.isSaved()) {
                try {
                    String filetype = fileInfo.getContentType();
                    byte[] b = Files.readAllBytes(fileInfo.getFile().toPath());
                    
                    Image i = new Image(filetype, b);
                    imagebean.addImage(i);
                    //get id
                    i = imagebean.update(i);
                    
                    //remove old profileImage
                    if(currentUser.getProfileImageId() > 0) {
                        imagebean.remove(currentUser.getProfileImageId());
                    }
                    
                    currentUser.setProfileImageId(i.getId());
                    currentUser = userbean.updateUser(currentUser);
                
                } catch (IOException ex) {
                    Logger.getLogger(SettingsBB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
      
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public long getProfileImageId() {
        return profileImageId;
    }

    public void setProfileImageId(long profileImageId) {
        this.profileImageId = profileImageId;
    }
}
