/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.BlogRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.CategoryRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.ImageRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Image;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.IBlogRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IEventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author sjoksda
 */
@Named
@SessionScoped
public class SearchBackingBean implements Serializable {

    @Inject
    private UserRegistryBean userBean;
    @Inject
    private EventRegistryBean eventBean;
    @Inject
    private ImageRegistryBean imageBean;
    @Inject
    private CategoryRegistryBean categoryBean;
    @Inject
    private BlogRegistryBean blogBean;
    private String user;
    private String event;
    private String blog;
    private String category;
    private String fname;
    private String lname;
    private List<TUser> users;
    private List<Event> events;
    private List<Category> categorys;
    private List<BlogEvent> blogEvents;

    public SearchBackingBean() {
        users = new ArrayList<>();
        events = new ArrayList<>();
        categorys = new ArrayList<>();
        blogEvents = new ArrayList<>();
    }

    public List<TUser> getAllUsers() {
        return users;
    }

    public List<TUser> getUsersStartingWith(String start) {
        List<TUser> matchingUsers = new ArrayList<>();
        List<TUser> allUsers = userBean.getAll();
        for (TUser u : allUsers) {
            if (u.getUsername().toLowerCase().startsWith(start.toLowerCase())) {
                matchingUsers.add(u);
            }
        }
        return matchingUsers;
    }

    public List<Event> getAllEvents() {
        return events;
    }

    public List<Event> getEventsStartingWith(String start) {
        List<Event> matchingEvents = new ArrayList<>();
        List<Event> allEvents = eventBean.getAll();
        for (Event e : allEvents) {
            if (e.getTitle().toLowerCase().startsWith(start.toLowerCase())) {
                matchingEvents.add(e);
            }
        }
        return matchingEvents;
    }

    public Image findImage(long id) {
        return imageBean.find(id);
    }


    public List<Category> getAllCategorys() {
        return categorys;
    }

    public List<Category> getCategorysStartingWith(String start) {
        List<Category> matchingCategorys = new ArrayList<>();
        List<Category> allCategorys = categoryBean.getAll();
        for (Category c : allCategorys) {
            if (c.getName().toLowerCase().startsWith(start.toLowerCase())) {
                matchingCategorys.add(c);
            }
        }
        return matchingCategorys;
    }

    public List<BlogEvent> getBlogEvents(long eventId) {
        return blogBean.getBlogEvents(eventId);
    }

    public List<BlogEvent> getAllBlogs() {
        return blogEvents;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getFname() {
        return fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getLname() {
        return lname;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void searchForUser() {
        users.clear();
        List<TUser> us = new ArrayList<>();

        if (user.length() != 0) {
            TUser u = userBean.getUser(user);
            if (u != null) {
                us.add(u);
            }
        } else if (fname.length() != 0 && lname.length() != 0) {
            for (TUser u1 : userBean.getAll()) {
                if (u1.getFname().toLowerCase().startsWith(fname.toLowerCase())
                        && u1.getLname().toLowerCase().startsWith(lname.toLowerCase())) {
                    us.add(u1);
                }
            }
        } else if (fname.length() != 0) {
            for (TUser u1 : userBean.getAll()) {
                if (u1.getFname().toLowerCase().startsWith(fname.toLowerCase())) {
                    us.add(u1);
                }
            }
        } else if (lname.length() != 0) {
            for (TUser u1 : userBean.getAll()) {
                if (u1.getLname().toLowerCase().startsWith(lname.toLowerCase())) {
                    us.add(u1);
                }
            }
        }
        users = us;
        clearAll();
        events.clear();
        categorys.clear();
        blogEvents.clear();
    }

    public void searchForCategory() {
        categorys.clear();
        List<Category> ct = new ArrayList<>();
        for (Category c : categoryBean.getAll()) {
            if (c.getName().toLowerCase().startsWith(category.toLowerCase())) {
                ct.add(c);
            }
        }
        categorys = ct;
        clearAll();
        users.clear();
        events.clear();
        blogEvents.clear();
    }

    public void searchForEvent() {
        events.clear();
        for (Event e : eventBean.getAll()) {
            if (e.getTitle().toLowerCase().startsWith(event.toLowerCase())) {
                events.add(e);
            }
        }
        clearAll();
        users.clear();
        categorys.clear();
        blogEvents.clear();
    }

    public void searchForBlog() {
        blogEvents.clear();
        for (BlogEvent e : blogBean.getAll()) {
            if (e.getTitle().toLowerCase().startsWith(blog.toLowerCase())) {
                blogEvents.add(e);
            }
        }
        clearAll();
        users.clear();
        events.clear();
        categorys.clear();

    }

    public void clearAll() {
        user = "";
        event = "";
        blog = "";
        category = "";
        fname = "";
        lname = "";
    }
} 