/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * Wrapping bean class for Event.
 * 
 * @author Erik
 */
@Named("ViewEvent")
@RequestScoped
public class ViewEventBB implements Serializable {
    
    @Inject 
    EventRegistryBean eventReg;
        
    private Event ev;
    
    public void addBlogEvent(BlogEvent be) {
        //ev.addBlogEvent(be);
    }
    
    public long getId() {
        return ev.getId();
    }
    
    public String getDescription() {
        return ev.getDescription();
    }
    
    public void setDescription(String description) {
        ev.setDescription(description);
    }
    
    public void setTitle(String title) {
        ev.setTitle(title);
    }
    
    public String getTitle() {
        return ev.getTitle();
    }
    
    public TUser getOwner() {
        return ev.getOwner();
    }
    
    public void setOwner(TUser owner) {
        ev.setOwner(owner);
    }
    
    public void setLocation(Location loc) {
        ev.setLocation(loc);
    }
    
    public Location getLocation() {
        return ev.getLocation();
    }
    
    public void setIsPrivate(boolean isPrivate) {
        ev.setIsPrivate(isPrivate);
    }
    
    public boolean getIsPrivate() {
        return ev.getIsPrivate();
    }
    
    public void setInternalEvent(Event ev) {
        this.ev = ev;
    }
    
    public Event getInternalEvent() {
        return ev;
    }

}
