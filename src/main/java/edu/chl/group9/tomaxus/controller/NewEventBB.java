package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.EventRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.CategoryRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.EventTimeRange;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * Wrapping bean class for Event.
 *
 * @author Erik
 */
@Named("newevent")
@RequestScoped
public class NewEventBB implements Serializable {

    @Inject
    EventRegistryBean eventReg;
    @Inject
    CategoryRegistryBean catReg;
    @Inject
    private UserRegistryBean userbean;
    
    private String description;
    private String title;
    private boolean isPrivate;
    private Long category;
    
    private String country;
    private String city;
    private String address;
    
    private String startDate;
    private String endDate;
    
    /*
     public void addBlogEvent(BlogEvent be) {
     ev.addBlogEvent(be);
     }
     
     public long getId() {
     return ev.getId();
     }*/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        Logger.getAnonymousLogger().log(Level.INFO, "Description set to{0}", description);
    }

    public void setTitle(String title) {
        this.title = title;
        Logger.getAnonymousLogger().log(Level.INFO, "Title set!{0}", title);
    }

    public String getTitle() {
        return title;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }
    
    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getCity(){
        return city;
    }
    
    public void setCity(String city){
        this.city = city;
    }
    
    public String getAddress(){
        return address;
    }
    public void setAddress(String address){
        this.address = address;
    }
    
    
    public String getStartDate() {
        return startDate;
    }
    
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }
    
    public String getEndDate(){
        return endDate;
    }
    
    public void setEndDate(String endDate){
        this.endDate = endDate;
    }
    
    public Map<String, Object> getCat2Val() {

        Map<String, Object> cat2val;

        List<Category> catList = catReg.getAll();
        cat2val = new LinkedHashMap<String, Object>();
        for (Category c : catList) {
            cat2val.put(c.getName(), c.getId());
        }
        return cat2val;
    }

    public void add() throws ParseException  {
        
        Event t = new Event();
        t.setDescription(description);
        t.setTitle(title);
        t.setLocation(new Location(country, city, address));
        
        TUser user = userbean.getUser((String)FacesContext.getCurrentInstance().getExternalContext()
                             .getSessionMap().get("ontherun"));
        t.setOwner(user);           
        t.addParticipant(user);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        Date startEventTime = sdf.parse(startDate);
        Date endEventTime = sdf.parse(endDate);
        t.setEventTimeRange(new EventTimeRange(startEventTime, endEventTime));
        
        Logger.getAnonymousLogger().log(Level.INFO, "Creating event{0}", t.toString());
        
        eventReg.addEvent(t);
        catReg.getCategory(category).addEvent(t);  
    }
}
