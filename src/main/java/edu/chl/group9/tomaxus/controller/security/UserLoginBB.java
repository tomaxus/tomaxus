package edu.chl.group9.tomaxus.controller.security;

import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean(name = "user")
@SessionScoped
public class UserLoginBB implements Serializable {

    private String name;
    private String password;
    private String firstname;
    private String lastname;
    private long id;
    private String gender;
    private int age;
    protected static final String CREDENTIAL = "ontherun";
    public static final String CREDENTIAL_USERID = "userid";
    private static final long serialVersionUID = 1L;
    
    private String errorMessage = "";
    

    public String getName() {
            return this.name;
    }
    
    public String getErrorMessage() {
        return errorMessage; 
    }
    
    public boolean getHasCustomImage() {
        return ((TUser) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get(CREDENTIAL_USERID)).getHasCustomImage();
    }
    
    public long getProfileImageId() {
        return ((TUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(CREDENTIAL_USERID) ).getProfileImageId();
    }
    
    public long getId() {
       return ((TUser) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get(CREDENTIAL_USERID) ).getId();
    }
    
    public TUser getUser() {
       return (TUser) FacesContext.getCurrentInstance().getExternalContext()
                                  .getSessionMap().get(CREDENTIAL_USERID);
    }
    
    public void setName(String newName) {
        this.name = newName;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    public String getFirstname() {
        return firstname;
    }
    
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    public String getLastname() {
        return lastname;
    }
    
    public String getGender(){
        return gender;
    }
    
    public void setGender(String gender){
        this.gender = gender;
    }
    
    public int getAge(){
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
            
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }

    public boolean isLoggedIn() {
        return FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get(CREDENTIAL_USERID) != null;
    }

    public void logout() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(CREDENTIAL);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(CREDENTIAL_USERID);
            
    }

    public String login() {
        ITUserRegistry userReg = Tomaxus.INSTANCE.getUserRegistry();
        if(userReg.validateUser(name, password)) {
            errorMessage = "";
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CREDENTIAL, this.name);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CREDENTIAL_USERID, userReg.getUser(name));
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("restricted/home.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(UserLoginBB.class.getName()).log(Level.SEVERE, null, ex);
                return "restricted/home.xhtml";
            }
             return "";
        } else {
            errorMessage = "Wrong username or password.";
            return "index.xhtml?faces-redirect=true";
        }
        
    }
    
    
    public String register() {
        ITUserRegistry userReg = Tomaxus.INSTANCE.getUserRegistry();
        if (userReg.checkUserName(name)) {
            TUser u = new TUser(name, firstname, lastname, gender, age, new Location("", "", ""));
            u.setPassword(password);
            userReg.add(u);
            errorMessage = "";
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CREDENTIAL, this.name);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CREDENTIAL_USERID, userReg.getUser(name));
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("restricted/home.xhtml");
            } catch (IOException ex) {
                return "";
            }
            return "";
        } else {
            return "";
        }
        /*
         * else error message
         */
    }
}
