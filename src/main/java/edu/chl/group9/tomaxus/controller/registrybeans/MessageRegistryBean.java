/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author peter
 */
@SessionScoped
public class MessageRegistryBean implements Serializable {

    public List<PersonalMessage> getAll() {
        return Tomaxus.INSTANCE.getMessageRegistry().getAll();
    }

    public void addMessage(PersonalMessage u) {
        Tomaxus.INSTANCE.getMessageRegistry().add(u);
    }

    public void deleteMessage(long id) {
        Tomaxus.INSTANCE.getMessageRegistry().remove(id);
    }
    
    public void updateMessage(PersonalMessage m) {
        Tomaxus.INSTANCE.getMessageRegistry().update(m);
    }

    public PersonalMessage find(long id) {
        return Tomaxus.INSTANCE.getMessageRegistry().find(id);
    }

    public List<PersonalMessage> getInboxForUser(TUser t) {
        return Tomaxus.INSTANCE.getMessageRegistry().getInboxForUser(t);
    }
    
    public List<PersonalMessage> getSentForUser(TUser t) {
        return Tomaxus.INSTANCE.getMessageRegistry().getSentForUser(t);
    }
        
    public int getNumberOfUnread(TUser t) {
        return Tomaxus.INSTANCE.getMessageRegistry().getNumberOfUnread(t);
    }
}
