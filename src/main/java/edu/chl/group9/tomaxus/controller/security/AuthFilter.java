package edu.chl.group9.tomaxus.controller.security;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthFilter implements Filter {

    private FilterConfig config;

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {
        if (((HttpServletRequest) req).getSession().getAttribute(
                UserLoginBB.CREDENTIAL) == null) {
             Logger.getAnonymousLogger().info("User is not logged in, redirecting...");
            ((HttpServletResponse) resp).sendRedirect("/tomaxus/index.xhtml");
        } else {
            Logger.getAnonymousLogger().info("User IS logged in OK.");
            Logger.getAnonymousLogger().info("User is on page: " + ((HttpServletRequest) req).getRequestURI() );            
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }
    
    @Override
    public void destroy() {
        config = null;
    }

}
