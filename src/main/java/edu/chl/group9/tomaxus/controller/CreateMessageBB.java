package edu.chl.group9.tomaxus.controller;

import edu.chl.group9.tomaxus.controller.registrybeans.UserRegistryBean;
import edu.chl.group9.tomaxus.controller.registrybeans.MessageRegistryBean;
import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * Bean for showing a specific TUser in profile.xhtml
 * 
 * Specify UserID before getters.
 * 
 * @author Erik
 */
@Named("createmessage")
@RequestScoped
public class CreateMessageBB implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    UserRegistryBean userReg;
    
    @Inject
    MessageRegistryBean messageReg;
    
    private UIComponent messageToTextField;
    

    
    private String messageTo;
    private String title;
    private String content;
    
    public void setMessageTo(String to) {
        this.messageTo = to;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getMessageTo() {
        return messageTo;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setMessageToTextField(UIComponent messageToTextField) {
        this.messageToTextField = messageToTextField;
    }

    public UIComponent getMessageToTextField() {
        return messageToTextField;
    }

    public String sendMessage() {
        if(messageTo != null && content != null && title != null) {
            if(!userReg.checkUserName(messageTo)) { 
                TUser to = userReg.getUser(messageTo);
                TUser me = (TUser) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userid");
                 messageReg.addMessage(new PersonalMessage(content, title, to, me));
                 return "inbox.xhtml?faces-redirect=true";
            } else {
                FacesMessage message = new FacesMessage("User not found.");
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(messageToTextField.getClientId(context), message);
                return "";
            }
        }
        return "";
    }
    
}
