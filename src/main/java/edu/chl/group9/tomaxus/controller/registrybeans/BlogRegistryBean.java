/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.controller.registrybeans;

import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author peter
 */
@SessionScoped
public class BlogRegistryBean implements Serializable {

    public List<BlogEvent> getAll() {
        return Tomaxus.INSTANCE.getBlogRegistry().getAll();
    }

    public void addBlogEvent(BlogEvent u) {
        Tomaxus.INSTANCE.getBlogRegistry().add(u);
    }

    public void deleteBlogEventById(long id) {
        Tomaxus.INSTANCE.getBlogRegistry().remove(id);
    }

    public void deleteBlogEvent(BlogEvent u) {
        Tomaxus.INSTANCE.getBlogRegistry().remove(u);
    }

    public BlogEvent updateBlogEvent(BlogEvent u) {
        return Tomaxus.INSTANCE.getBlogRegistry().update(u);
    }

    public BlogEvent find(long id) {
        return Tomaxus.INSTANCE.getBlogRegistry().find(id);
    }

    public List<BlogEvent> getBlogEvents(long eventId) {
        return Tomaxus.INSTANCE.getBlogRegistry().getBlogsForEvent(eventId);
    }
}
