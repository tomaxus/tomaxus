package edu.chl.group9.tomaxus.tmp;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.IBlogRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ICategoryRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IEventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.util.ArrayList;

/**
 * Inject testdata into database, for testing only.
 *
 * @author Erik
 */
public class TestConfiguration {

    private static boolean cleanOldData = true;

    public static void injectTestData() {
        ITUserRegistry userReg = Tomaxus.INSTANCE.getUserRegistry();
        IEventRegistry eventReg = Tomaxus.INSTANCE.getEventRegistry();
        ICategoryRegistry cateReg = Tomaxus.INSTANCE.getCategoryRegistry();
        IBlogRegistry blogReg = Tomaxus.INSTANCE.getBlogRegistry();

        if (cleanOldData) {
            for (BlogEvent b : blogReg.getAll()) {
                blogReg.remove(b);
            }

            for (TUser t : userReg.getAll()) {
                userReg.remove(t);
            }

            for (Event t : eventReg.getAll()) {
                eventReg.remove(t);
            }

            for (Category c : cateReg.getAll()) {
                cateReg.remove(c);
            }
        }

        TUser t1 = new TUser("test", "Test", "Testsson", "Female", 32, new Location("Lidköping", "Sweden", "Testgatan 88"));
        t1.setPassword("test");

        TUser t2 = new TUser("Per", "Per", "Persson", "Male", 17, new Location("Persholmen", "Pers-rike", "Pergatan per"));
        t2.setPassword("per");

        TUser gubbe1 = new TUser("Evert", "Evertsson", "Karlsson", "Male", 83, new Location("Stockholm", "Sweden", "Pergatan per"));
        TUser gubbe2 = new TUser("Sven", "Gunnar", "Gunnarson", "Male", 12, new Location("Växsjö", "Sweden", "Pergatan per"));
        TUser gubbe3 = new TUser("Clara", "Tiara", "Gunnarson", "Female", 27, new Location("London", "Great Britain", "Pergatan per"));
        TUser gubbe4 = new TUser("Bertil", "fiskarn", "Jonsson", "Male", 99, new Location("New York", "USA", "Pergatan per"));
        TUser gubbe5 = new TUser("Trallarn", "Gustav", "Svensson", "Male", 19, new Location("New York", "USA", "Dastreet 21"));
        TUser gubbe6 = new TUser("Petarn", "Sven", "Svensson", "Male", 33, new Location("Växjö", "Sverige", "Likstigen 1"));
        TUser gubbe7 = new TUser("Byggarn", "Erik", "Larsson", "Male", 45, new Location("Norrköping", "Sveige", "Kottvägen 2"));
        TUser gubbe8 = new TUser("Spjutet", "Lisa", "Cos", "Female", 22, new Location("Stockholm", "Sverige", "Vattenvägen 6"));
        TUser gubbe9 = new TUser("Gus", "Jonas", "Gustavsson", "Male", 38, new Location("Halmstad", "Sverige", "Sjövägen 86"));
        TUser gubbe10 = new TUser("Ekan", "Erik", "Gunnarsson", "Male", 44, new Location("Limmared", "Sverige", "Norrgatan 43"));




        Event e1 = new Event("Löpning", "Löpning ingår som en naturlig del i nationella mästerskapen,"
                + "kontinentala mästerskapen och världsmästerskapen i friidrott. Dessutom "
                + "finns den som del av friidrotten vid sommar-OS. Där tävlas både i banlöpning"
                + "inne på stadion och i maraton på en stadsbana utefter gator och vägar."
                + "De viktigaste svenska tävlingarna är Lidingöloppet, Stockholm Marathon och Göteborgsvarvet."
                + "Men jag ska springa 10mil",
                new Location("Göteborg", "Sverige", "Bergen 8"), gubbe1, true);


        Event e2 = new Event("Pussas", "Min första kyss var förra veckan! Jag var i skolan och hittade en lapp på min bänk där det stod "
                + "Möt mig bakom skolan efter sista lektionen. När skolan var slut för dagen gick jag till mötesplatsen, och"
                + "där stod klassens snyggaste kille. Han såg mig djupt i ögonen och frågade om vi skulle bli tillsammans."
                + "Jag svarade JA kanske lite för entusiastiskt, och så kysste han mig LÄNGE. Nu vill jag ha en andra kyss",
                new Location("Stockholm", "Sverige", "Bratzgatan 08"),
                gubbe2, false);

        Event e3 = new Event("Fotspikning", "Jag har alltid gillat att spika mig i foten, jag vill nu klara att spika mig i foten utan att få ont",
                new Location("Malmö", "Sverige", "Hamnen 5"),
                gubbe3, true);

        Event e4 = new Event("Fiska", "Fånga en haj med metspö är något som jag bara måste pröva. Vanligtvis brukar jag plocka dom med"
                + "händerna men då detta är för lätt så vill jag fånga en med ett metspö",
                new Location("Kapstaden", "Sydafrika", "Oceanstreet 51"),
                gubbe4, true);

        Event e5 = new Event("Fylla 20", "Jag fyller 20 om 3 dagar och nu är läget såhär att jag opererar mej snart så jag får inte röka/dricka "
                + "eller nåt sånt.. måste hålla mig flera veckor innan och efter operationen. Så jag ska ha en nykter fest på lördag",
                new Location("Luleå", "Sverige", "Vintergatan 10"),
                gubbe5, true);

        Event e6 = new Event("Pussa en kille", "Jag vill pussa en kille innan jag fyller 7, ingen annan i min klass har pussat en kille"
                + " så jag vill vara den första",
                new Location("Köpenhamn", "Danmark", "Vissengrend 3"),
                gubbe6, true);

        Event e7 = new Event("Lära mig sy", "Jag har precis lärt mig att sy och nu skulle jag vilja lära mig sy en fin väska till min mamma",
                new Location("Haninge", "Sverige", "Likgränd 41"),
                gubbe6, true);

        Event e8 = new Event("Gå på händer", "Jag har blivit jättestark i mina händer så jag skulle vilja kunna gå på händer mellan malmö och happaranda",
                new Location("Malmö", "Sverige", "Tallvägen 5"),
                gubbe7, true);

        Event e9 = new Event("Springa väldigt snabbt", " Geparden är det nu levande snabbaste däggdjuret och kan korta sträckor springa i upp till 114 km/h " +
                ". Mitt mål är att kunna springa fortare än en gepard vilket gör att JAG blir världens snabbaste däggdjur",
                new Location("Kapstaden", "Sydafrika", "Oceanstreet 51"),
                gubbe8, true);

        Event e10 = new Event("Simma", "En indisk pojke kom in till sjukhuset med svåra smärtor i urinvägarna. En av hans akvariefiskar hade simmat upp i " +
                            "hans penis och in i urinblåsan. Nu vill jag lära mig simma som en fisk",
                new Location("Kapstaden", "Sydafrika", "Oceanstreet 51"),
                gubbe9, true);

        Event e11 = new Event("Dyka", "Dyka med vithaj",
                new Location("Kapstaden", "Sydafrika", "Middleway 33"),
                gubbe10, true);

        Event e12 = new Event("Duka", "Duka upp bord i hela ullevi",
                new Location("Göteborg", "Sverige", "Göteborgsvägen 34"),
                gubbe1, true);

        Event e13 = new Event("Climb Mount Everest", "Mount Everest is the Earth's highest "
                + "mountain, with a peak at 8,848 metres (29,029 ft) above sea level. "
                + "It is located in the Mahalangur section of the Himalayas. The "
                + "international border between China and Nepal runs across the precise "
                + "summit point. Its massif includes neighboring peaks "
                + "Lhotse, 8,516 m (27,940 ft); Nuptse, 7,855 m (25,771 ft); and Changtse, "
                + "7,580 m (24,870 ft).",
                new Location("Himalayas", "Nepal", "Mnt.Everest"), t2, false);
        e5.addParticipant(gubbe4);
        e5.addParticipant(t1);
        e2.addParticipant(t1);
        e3.addParticipant(gubbe1);
        e5.addParticipant(gubbe3);
        e13.addParticipant(gubbe8);
        e13.addParticipant(gubbe3);
        e13.addParticipant(gubbe4);
        e13.addParticipant(gubbe5);
        e11.addParticipant(gubbe1);
        e11.addParticipant(t1);
        e11.addParticipant(gubbe2);
        e11.addParticipant(gubbe7);
        e8.addParticipant(gubbe1);
        e8.addParticipant(gubbe3);
        e8.addParticipant(gubbe5);
        e8.addParticipant(gubbe9);
        e9.addParticipant(gubbe2);
        e9.addParticipant(gubbe4);
        e9.addParticipant(gubbe5);
        e9.addParticipant(gubbe7);

        Category c1 = new Category("Family, Friends and Relationships", "What you need in your social life to feel successful", new ArrayList<Event>());
        Category c2 = new Category("Learn", "A skill, a trick, a lesson etc", new ArrayList<Event>());
        Category c3 = new Category("Create", "Everything from crafts to wonders", new ArrayList<Event>());
        Category c4 = new Category("Adventures & Travel", "Special occasions and places you need to experience", new ArrayList<Event>());
        Category c5 = new Category("Education & Career", "Climb that ladder! Or just be you", new ArrayList<Event>());
        Category c6 = new Category("Legacy", "Passing on knowledge, values and matter, leaving a footprint to future mankind, big or small", new ArrayList<Event>());
        Category c7 = new Category("Comebacks", "From struggle to success. A seemingly small thing can be a great victory.", new ArrayList<Event>());
        Category c8 = new Category("Physical & Mental Challenges", "To run a race, stand in the rain, to eat your daddys food or to outsmart your sister in chess. What's your challenge?", new ArrayList<Event>());
        Category c9 = new Category("Volonteering", "Everybody can do something.", new ArrayList<Event>());
        Category c10 = new Category("Possessions and belongings", "Do you dream about a certain car or a new surf board? Write it down here.", new ArrayList<Event>());

        BlogEvent b1 = new BlogEvent("Routes", "Mt. Everest has two main climbing routes, the "
                + "southeast ridge from Nepal and the north ridge from Tibet, as well "
                + "as many other less frequently climbed routes. Of the two main "
                + "routes, the southeast ridge is technically easier and is the more "
                + "frequently used route. It was the route used by Edmund Hillary and "
                + "Tenzing Norgay in 1953 and the first recognized of fifteen routes to "
                + "the top by 1996. This was, however, a route decision dictated more"
                + " by politics than by design as the Chinese border was closed to the "
                + "western world in the 1950s after the People's Republic of China invaded"
                + " Tibet.", false, e13, t2);
        BlogEvent b2 = new BlogEvent("Dangers", "Some climbers have reported life-threatening thefts "
                + "from supply caches. Vitor Negrete, the first Brazilian to climb Everest "
                + "without oxygen and part of David Sharp's party, died during his descent, "
                + "and theft from his high-altitude camp may have contributed. In addition"
                + "to theft, the 2008 book High Crimes by Michael Kodas describes unethical"
                + " guides and Sherpas, prostitution and gambling at the Tibet Base Camp, "
                + "fraud related to the sale of oxygen bottles, and climbers collecting "
                + "donations under the pretense of removing trash from the mountain.",
                false, e13, t1);
        BlogEvent b3 = new BlogEvent("Records", "The youngest person to climb Mount Everest was "
                + "13-year-old Jordan Romero in May 2010 from the Tibetan side. "
                + "His ascent, as part of an apparent race to bring younger and younger "
                + "children to the mountain (shortly after Romero's ascent, Pemba Dorjie "
                + "Sherpa announced plans to bring his 9 year old son to the summit), "
                + "triggered a wave of criticism that prompted Chinese authorities to "
                + "establish age limits on Mt Everest. At the present time, China no "
                + "longer grants permits to prospective climbers under 18 or over 60. "
                + "Nepal sets the minimum age at 16 but has no maximum age.",
                false, e13, t2);

        c8.addEvent(e1);
        c8.addEvent(e2);
        c4.addEvent(e3);
        c4.addEvent(e4);
        c1.addEvent(e5);
        c1.addEvent(e6);
        c2.addEvent(e7);
        c8.addEvent(e8);
        c8.addEvent(e9);
        c8.addEvent(e10);
        c4.addEvent(e11);
        c3.addEvent(e12);
        c4.addEvent(e13);



        userReg.add(gubbe1);
        userReg.add(gubbe2);
        userReg.add(gubbe3);
        userReg.add(gubbe4);
        userReg.add(gubbe6);
        userReg.add(gubbe7);
        userReg.add(gubbe8);
        userReg.add(gubbe9);
        userReg.add(gubbe10);
        userReg.add(gubbe5);
        userReg.add(t2);
        userReg.add(t1);


        eventReg.add(e1);
        eventReg.add(e2);
        eventReg.add(e3);
        eventReg.add(e4);
        eventReg.add(e5);
        eventReg.add(e6);
        eventReg.add(e7);
        eventReg.add(e8);
        eventReg.add(e9);
        eventReg.add(e10);
        eventReg.add(e11);
        eventReg.add(e12);
        eventReg.add(e13);




        cateReg.add(c1);
        cateReg.add(c2);
        cateReg.add(c3);
        cateReg.add(c4);
        cateReg.add(c5);
        cateReg.add(c6);
        cateReg.add(c7);
        cateReg.add(c8);
        cateReg.add(c9);
        cateReg.add(c10);

        blogReg.add(b1);
        blogReg.add(b2);
        blogReg.add(b3);


        t1.addFriend(gubbe1);
        t1.addFriend(gubbe2);
        t1.addFriend(gubbe3);
        t1.addFriend(gubbe4);

        t2.addFriend(gubbe1);
        t2.addFriend(gubbe2);
        t2.addFriend(gubbe3);


        gubbe6.addFriend(gubbe1);
        gubbe6.addFriend(gubbe2);
        gubbe6.addFriend(gubbe3);
        gubbe6.addFriend(gubbe4);

        gubbe7.addFriend(gubbe1);
        gubbe7.addFriend(gubbe2);
        gubbe7.addFriend(gubbe3);
        gubbe7.addFriend(gubbe4);

        gubbe7.addFriend(gubbe1);
        gubbe7.addFriend(gubbe2);
        gubbe7.addFriend(gubbe3);
        gubbe7.addFriend(gubbe4);

        userReg.update(t1);
        userReg.update(t2);
        userReg.update(gubbe6);
        userReg.update(gubbe7);



    }
}
