/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.tmp;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
/**
 * Web application lifecycle listener.
 * @author Erik
 */

@WebListener()
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        TestConfiguration.injectTestData();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
