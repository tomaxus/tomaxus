/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Class representing a message between two TUsers.
 * 
 * Contains a title and message content.
 * 
 * @author Erik
 */
@Entity
public class PersonalMessage implements Serializable {
    
    @Column(length=1000)
    private String messageContent;
    
    @Column(length=255)
    private String title;
    
    @OneToOne
    private TUser messageTo;
    
    @OneToOne
    private TUser messageFrom;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private boolean isRead;

    public PersonalMessage() {
    }
    
    public PersonalMessage(String messageContent, String title, TUser messageTo, TUser messageFrom) {
        this.messageContent = messageContent;
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;
        this.title = title;
        isRead = false;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public long getId() {
        return id;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessageTo(TUser messageTo) {
        this.messageTo = messageTo;
    }

    public void setMessageFrom(TUser messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public String getTitle() {
        return title;
    }

    public TUser getMessageTo() {
        return messageTo;
    }

    public TUser getMessageFrom() {
        return messageFrom;
    }

    public boolean getIsRead() {
        return isRead;
    }
    
    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "PersonalMessage{" + "title=" + title + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.messageContent);
        hash = 11 * hash + Objects.hashCode(this.title);
        hash = 11 * hash + Objects.hashCode(this.messageTo);
        hash = 11 * hash + Objects.hashCode(this.messageFrom);
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersonalMessage other = (PersonalMessage) obj;
        if (!Objects.equals(this.messageContent, other.messageContent)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.messageTo, other.messageTo)) {
            return false;
        }
        if (!Objects.equals(this.messageFrom, other.messageFrom)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
