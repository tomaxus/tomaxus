/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.Image;
import java.util.List;

/**
 *
 * @author temp
 */
public class ImageRegistry extends AbstractRegistryDAO<Image> 
                                implements IImageRegistry {
   
    public static IImageRegistry newInstance(String puName) {
        return new ImageRegistry(puName);
    }
   
    private ImageRegistry(String puName) {
        super(puName, Image.class);
    }
    
    @Override
    public List<Image> search(String t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
