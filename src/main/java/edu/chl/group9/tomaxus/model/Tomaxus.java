/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import edu.chl.group9.tomaxus.model.dbaccess.BlogRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.CategoryRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.EventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IBlogRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ICategoryRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.TUserRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IEventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IImageRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IMessageRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ImageRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.MessageRegistry;

/**
 * Facade-pattern.
 * 
 * @author peter
 */
public enum Tomaxus {
    
    INSTANCE;
    private static final String puName = "tomaxus";
    private final ITUserRegistry userRegistry = TUserRegistry.newInstance(puName);
    private final IEventRegistry eventRegistry = EventRegistry.newInstance(puName);
    private final IBlogRegistry blogRegistry = BlogRegistry.newInstance(puName);
    private final ICategoryRegistry categoryRegistry = CategoryRegistry.newInstance(puName);
    private final IImageRegistry imageRegistry = ImageRegistry.newInstance(puName);
    private final IMessageRegistry messageRegistry = MessageRegistry.newInstance(puName);
     
    private Tomaxus() {
    }

    public ITUserRegistry getUserRegistry() {
        return userRegistry;
    }
    
    public IEventRegistry getEventRegistry() {
        return eventRegistry;
    }
    
    public IBlogRegistry getBlogRegistry() {
        return blogRegistry;
    }

    public ICategoryRegistry getCategoryRegistry(){
        return categoryRegistry;
    }

    public IImageRegistry getImageRegistry() {
        return imageRegistry;
    }
    
    public IMessageRegistry getMessageRegistry() {
        return messageRegistry;
    }
}
