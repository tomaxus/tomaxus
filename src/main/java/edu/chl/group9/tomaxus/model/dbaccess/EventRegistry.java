/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Erik
 */
public class EventRegistry extends AbstractRegistryDAO<Event> implements IEventRegistry {

    public static IEventRegistry newInstance(String puName) {
        return new EventRegistry(puName);
    }
    
    public EventRegistry(String puName) {
        super(puName, Event.class);
    }

    @Override
    public List<TUser> getUsersFromEvent(long eventId) {
        EntityManager em = null;
        List<TUser> found = null;
        try {
            em = emf.createEntityManager();
            String query = "select u from Event u WHERE u.id = " + eventId+";";
            TypedQuery<Event> q = em.createQuery(query, clazz);
            found = q.getSingleResult().getParticipants();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return found;
    }
        

    @Override
    public List<Event> getEventsFromUser(long userId) {
        List<Event> allEvents = getAll();
        List<Event> result = new ArrayList<>();
        for(Event e : allEvents) {
            for(TUser t : e.getParticipants()){
                if(t.getId() == userId){
                    result.add(e);
                }
            }
        }
        return result;
        /*
        String query = "select u from Event u WHERE Event:participants IN :participants";
        TypedQuery<Event> p = emf.createEntityManager().createQuery(query, clazz);
        List<Event> list = p.getResultList();
        return list;*/
    }

    public EventRegistry(String puName, Class<Event> clazz) {
        super(puName, clazz);
    }
    
    @Override
    public List<Event> search(String t) {
        EntityManager em = null;
        List<Event> list = null;
        try {
            em = emf.createEntityManager();
            String query = "select p from Event p where p.title LIKE '%"+t+"%'";
            TypedQuery<Event> q = em.createQuery(query, clazz);
            list = q.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return list;
    }
}
