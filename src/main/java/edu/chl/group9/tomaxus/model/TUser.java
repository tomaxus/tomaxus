 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * A class representing a user.
 * 
 * 
 * @author group9
 */
@Entity
public class TUser implements Serializable, Comparable<TUser> {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String username;
    private String fname;
    private String lname;
    private int age;
    private String gender;
    private String password;
    
    @Embedded
    private Location loc = new Location();
    @OneToMany
    private List<TUser> friends;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date timeJoined;

    private long profileImageId;
    
    public TUser() {
    }
    
    
    public TUser(String username, String fname, String lname) {
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        friends = new ArrayList<>();
    }
    
    public TUser(String username, String fname, String lname, String gender, int age, Location l) {
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.age = age;
        this.loc = l;
        friends = new ArrayList<>();
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
       return password;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setAge(int age){
        this.age = age;
    }
    
    public int getAge(){
        return age;
    }
    
    public String getGender(){
        return gender;
    }
    
    public void setGender(String gender){
        this.gender = gender;
    }
    
    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }
    
    public long getProfileImageId() {
        return profileImageId;
    }

    public void setProfileImageId(long profileImageId) {
        this.profileImageId = profileImageId;
    }
    
     /**
     * Removes a friend from the friends-list.
     * 
     * Do a update on the TUser-object afterwards.
     * 
     * @param id TUser id to remove
     * @return true if it was true.
     * otherwise false
     */
    public boolean removeFriend(TUser other) {
        friends.remove(other);
        return true;
    }
    public List<TUser> getFriends() {
        return friends;
    }
    
    public boolean isFriendsWith(TUser other) {
        return friends.contains(other);
    }
        
    public boolean getHasCustomImage() {
        return profileImageId != 0;
    }
    
    public void clearFriends(){
        friends.clear();
    }
    
    public void addFriend(TUser user) {
        friends.add(user);
    }
    
    public Date getTimeJoined() {
        return timeJoined;
    }
    
    public void setTimeJoined(Date time) {
        this.timeJoined = time;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TUser) {
            TUser other = (TUser) obj;
             return hashCode() == other.hashCode();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.username);
        hash = 97 * hash + Objects.hashCode(this.fname);
        hash = 97 * hash + Objects.hashCode(this.lname);
        return hash;
    }

    @Override
    public int compareTo(TUser o) {
        return (fname+lname).compareTo(o.fname+o.lname);
    }

    @Override
    public String toString() {
        return username;
    }
    
    
    
    
}
