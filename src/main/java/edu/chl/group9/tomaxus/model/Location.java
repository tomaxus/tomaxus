/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * Class representing a location.
 *
 * @author Erik
 */
@Embeddable
public class Location implements Serializable {

    private String city;
    private String country;
    private String streetAddress;

    public Location() {
    }

    public Location(String country, String city, String streetAddress) {
        this.country = country;
        this.city = city;
        this.streetAddress = streetAddress;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    @Override
    public String toString() {
        return city + ", " + country;
    }
}
