/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface ICategoryRegistry extends IAbstractRegistry<Category>{
    
    public List<Event> getEventsFromCategory(long categoryId);
    public List<Event> getEventsFromCategory(String categoryName);
    public List<Category> getCategoryFromEvent(long eventId);
    public Category getCategory(long categoryId);
    public Category getCategory(String name);

}

