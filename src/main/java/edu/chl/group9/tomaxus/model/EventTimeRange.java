/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * Class for representing a time range in an event.
 * 
 * @author Erik
 */
@Entity
public class EventTimeRange implements Serializable  {
    String timeTitle;
    String timeDescription;
    @Temporal(TemporalType.DATE)
    Date startEventTime;
    @Temporal(TemporalType.DATE)
    Date endEventTime;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    public EventTimeRange() {
        
    }
    
    public EventTimeRange(Date startDate, Date endDate) {
        startEventTime = startDate; 
        endEventTime = endDate;
    }

    public Long getId() {
        return id;
    }
    
    public void setTitle(String title) {
        this.timeTitle = title;
    }
    
    public String getTitle() {
        return timeTitle;
    }
    
     public void setDescription(String description) {
        this.timeDescription = description;
    }
    
    public String getDescription() {
        return timeDescription;
    }
    
    public Date getStartTime() {
        return startEventTime;
    }
    
    public void setStartTime(Date start) {
        this.startEventTime = start;
    }
    
    public Date getEndTime() {
        return endEventTime;
    }
    
    public void setEndTime(Date end) {
        this.endEventTime = end;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventTimeRange other = (EventTimeRange) obj;
        if (!Objects.equals(this.timeTitle, other.timeTitle)) {
            return false;
        }
        if (!Objects.equals(this.timeDescription, other.timeDescription)) {
            return false;
        }
        if (!Objects.equals(this.startEventTime, other.startEventTime)) {
            return false;
        }
        if (!Objects.equals(this.endEventTime, other.endEventTime)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }   
       
}
