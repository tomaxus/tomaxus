/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author temp
 */
public class MessageRegistry extends AbstractRegistryDAO<PersonalMessage> 
                                implements IMessageRegistry {
   
    public static IMessageRegistry newInstance(String puName) {
        return new MessageRegistry(puName);
    }
   
    private MessageRegistry(String puName) {
        super(puName, PersonalMessage.class);
    }
    
    @Override
    public List<PersonalMessage> search(String t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<PersonalMessage> getInboxForUser(TUser t) {
      List<PersonalMessage> all = getAll();
       List<PersonalMessage> result = new ArrayList<>();
       for(PersonalMessage pm : all) {
           if(pm.getMessageTo().equals(t)) {
               result.add(pm);
           }
       }
       return result;
    }

    @Override
    public List<PersonalMessage> getSentForUser(TUser t) {
      List<PersonalMessage> all = getAll();
       List<PersonalMessage> result = new ArrayList<>();
       for(PersonalMessage pm : all) {
           if(pm.getMessageFrom().equals(t)) {
               result.add(pm);
           }
       }
       return result;
    }

    @Override
    public int getNumberOfUnread(TUser t) {
       List<PersonalMessage> all = getInboxForUser(t);
       List<PersonalMessage> result = new ArrayList<>();
       for(PersonalMessage pm : all) {
           if(!pm.getIsRead()) {
               result.add(pm);
           }
       }
       return result.size();
    }
}
