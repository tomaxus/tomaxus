/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daniel
 */
public class CategoryRegistry extends AbstractRegistryDAO<Category> implements ICategoryRegistry {



    
    public static ICategoryRegistry newInstance(String puName){
        return new CategoryRegistry(puName);
    }
    
    public CategoryRegistry(String puName){
        super(puName, Category.class);
    }
            
    @Override
    public List<Category> search(String t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Event> getEventsFromCategory(long categoryId) {
        List<Event> result = super.find(categoryId).getEvents();
        if(result != null) {
            return result;
        }
        return new ArrayList<>();
        /*EntityManager em = null;
        List<Event> found = null;
        try {
            em = emf.createEntityManager();
            String query = "select c from Category c WHERE c.id = " + categoryId+";";
            TypedQuery<Category> q = em.createQuery(query, clazz);
            found = q.getSingleResult().getEvents();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return found;*/
    }
    
    @Override
    public List<Event> getEventsFromCategory(String categoryName) {
        EntityManager em = null;
        List<Event> found = null;
        try {
            em = emf.createEntityManager();
            String query = "select c from Category c WHERE c.name = " + categoryName+";";
            TypedQuery<Category> q = em.createQuery(query, clazz);
            found = q.getSingleResult().getEvents();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return found;
    } 

    @Override
    public Category getCategory(long categoryId) {
        EntityManager em = null;
        Category c = null;
        try {
            em = emf.createEntityManager();
            String query = "select c from Category c WHERE c.id = " + categoryId + ";";
            TypedQuery<Category> q = em.createQuery(query, clazz);
            c = q.getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return c;       
    }

    @Override
    public Category getCategory(String name) {
        EntityManager em = null;
        Category cat = null;
        try {
            em = emf.createEntityManager();
            String query = "select c from Category c WHERE c.name = " + name + ";";
            TypedQuery<Category> q = em.createQuery(query, clazz);
            cat = q.getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }

        return cat;
    }
    
    @Override
    public List<Category> getCategoryFromEvent(long eventId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
