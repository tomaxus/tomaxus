/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import java.util.List;

/**
 *
 * @author Erik
 */
public interface IEventRegistry extends IAbstractRegistry<Event> {

    public List<TUser> getUsersFromEvent(long eventId);
    public List<Event> getEventsFromUser(long userId);
}
