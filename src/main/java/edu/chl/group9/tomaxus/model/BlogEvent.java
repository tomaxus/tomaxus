/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Johanna
 */

@Entity
public class BlogEvent implements Serializable, Comparable<BlogEvent> {  
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(length=1000)
    private String blogContent;
    private List<String> imageURL;
    private boolean isPrivate;
    private String title;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date timeCreated;
    
    @OneToOne
    private Event belongsToEvent;
    @OneToOne
    private TUser author;
    
    public BlogEvent(){
        imageURL = new ArrayList<>();
        timeCreated = Calendar.getInstance().getTime();
    }
    
        
    public BlogEvent(String title, String content, boolean isPrivate, 
            Event event, TUser author) {
        this.title = title;
        this.blogContent = content;
        this.isPrivate = isPrivate;
        this.belongsToEvent = event;
        this.author = author;
        imageURL = new ArrayList<>();
        timeCreated = Calendar.getInstance().getTime();
    }
    
     public void addImage(String iURL) {
        imageURL.add(iURL);
    }
    
    public void removeImage(String iURL) {
        imageURL.remove(iURL);
    }
    
    public List<String> getImages() {
        return imageURL;
    }

    public Long getId() {
        return id;
    }
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getBlogContent() {
        return blogContent;
    }

    public boolean getPrivate() {
        return isPrivate;
    }
    
    public void setPrivate(boolean isPrivate){
        this.isPrivate = isPrivate;
    }

    public void setContent(String c) {
        this.blogContent = c;
    }

    public Event getEvent() {
        return belongsToEvent;
    }
    
    public void setEvent(Event e) {
        this.belongsToEvent = e;
    }
    
    public Date getTimeCreated() {
        return (Date) timeCreated.clone();
    }
    
    public void setTimeCreated(Date time) {
        this.timeCreated = time;
    }

    public TUser getAuthor() {
        return author;
    }

    public void setAuthor(TUser author) {
        this.author = author;
    }

    @Override
    public int compareTo(BlogEvent o) {
       if(o != null) {
        return timeCreated.compareTo(o.timeCreated);
       } else {
           return -1;
       }       
    }
    
}