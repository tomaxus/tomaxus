/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.PersonalMessage;
import edu.chl.group9.tomaxus.model.TUser;
import java.util.List;

/**
 *
 */
public interface IMessageRegistry extends IAbstractRegistry<PersonalMessage> {
    public List<PersonalMessage> getInboxForUser(TUser t);
    public List<PersonalMessage> getSentForUser(TUser t);
    public int getNumberOfUnread(TUser t);
}
