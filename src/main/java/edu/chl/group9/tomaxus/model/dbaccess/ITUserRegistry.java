/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.TUser;

/**
 *
 * @author peter
 */
public interface ITUserRegistry extends IAbstractRegistry<TUser> {
    
    public TUser getUser(String nickname);
    public TUser getUser(Long id);

    public boolean validateUser(String username, String password);
    public boolean checkUserName(String username);
   
    
}
