/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.TUser_;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 *
 * @author peter
 */
public class TUserRegistry extends AbstractRegistryDAO<TUser> 
                                implements ITUserRegistry {

   
    public static ITUserRegistry newInstance(String puName) {
        return new TUserRegistry(puName);
    }
   
    private TUserRegistry(String puName) {
        super(puName, TUser.class);
    }

    @Override
    public TUser getUser(String nickname) {
        EntityManager em = null;
        TUser u = null;
        try {
            em = emf.createEntityManager();
            String query =
                    "select c from TUser c where c.username = :username";
            TypedQuery<TUser> q = em.createQuery(query, TUser.class);
            q.setParameter("username", nickname);
            u = q.getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return u;
    }

    @Override
    public TUser getUser(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }   
    
    @Override
    public List<TUser> search(String s) {
        EntityManager em = null;
        List<TUser> found = new ArrayList<>();
        try {
            em = emf.createEntityManager();
            CriteriaQuery<TUser> cq = em.getCriteriaBuilder().createQuery(TUser.class);
            Root<TUser> us = cq.from(TUser.class);
            cq.select(us);
            Path<String> name = us.get(TUser_.fname);
            cq.where(em.getCriteriaBuilder().equal(name, s));
            TypedQuery<TUser> q = em.createQuery(cq);
            found.addAll(q.getResultList());

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return found;
    }

    @Override
    public boolean validateUser(String username, String password) {
        List<TUser> all = getAll();
        for(TUser t: all) {
            if(t.getUsername().equals(username) && t.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkUserName(String username) {
        List<TUser> all = getAll();
        for(TUser t: all) {
            if(t.getUsername().equals(username)) {
                return false;
            }
        }
        return true;
    }

}
