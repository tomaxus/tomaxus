package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.BlogEvent;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author peter
 */
public class BlogRegistry extends AbstractRegistryDAO<BlogEvent> 
                                implements IBlogRegistry {

    public static IBlogRegistry newInstance(String puName) {
        return new BlogRegistry(puName);
    }
   
    
    private BlogRegistry(String puName) {
        super(puName, BlogEvent.class);
    }
    
    @Override
    public List<BlogEvent> getBlogsForEvent(long id) {

        EntityManager em = null;
        List<BlogEvent> found = null;
        try {
            em = emf.createEntityManager();
            String eventsById = "select b from BlogEvent b where b.belongsToEvent.id = ?1";
            TypedQuery<BlogEvent> q = em.createQuery(eventsById, BlogEvent.class);
            q.setParameter(1, id);
            found = q.getResultList();
        } catch (Exception ex) {

        } finally {
            if (em != null) {
                em.close();
            }
        }
        return found;
    }


    @Override
    public List<BlogEvent> search(String t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
