/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class representing an event.
 * 
 * Default sorting is by title ascending.
 *
 * @author Erik
 */
@Entity
public class Event implements Serializable, Comparable<Event> {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    @Column(length=1000)
    private String eventDescription;
    @Column(length=100)
    private String eventTitle;
    private boolean isPrivate;
    private boolean isAccomplished;
    
    public static final String DEFAULT_EVENT_IMAGE = "../images/profile/Sample_Profile_Image.png";
    
    @Embedded
    private Location loc;
    @OneToOne
    private TUser owner;
    @OneToMany
    private List<TUser> participants;
    @OneToOne(cascade= CascadeType.ALL)
    private EventTimeRange timerange = new EventTimeRange();
    @Temporal(TemporalType.DATE)
    private Date timeCreated;
    private long imageId;
    


    
    public Event() {
        participants = new ArrayList<>();
        timeCreated = Calendar.getInstance().getTime();
    }
    
    public Event(String title, String description, Location loc, TUser owner, boolean isPrivate){
        this.eventTitle = title;
        this.eventDescription = description;
        this.loc = loc;
        this.owner = owner;
        this.isPrivate = isPrivate;    
        this.participants = new ArrayList<>();
        participants.add(owner);
        timeCreated = Calendar.getInstance().getTime();
    }
    
    public long getId() {
        return id;
    }
    
    public String getDescription() {
        return eventDescription;
    }
    
    public void setDescription(String description) {
        this.eventDescription = description;
    }
    
    public void setTitle(String title) {
        this.eventTitle = title;
    }
    
    public String getTitle() {
        return eventTitle;
    }
    
    public TUser getOwner() {
        return owner;
    }
    
    public void setOwner(TUser owner) {
        this.owner = owner;
    }

    public List<TUser> getParticipants() {
        return participants;
    }
    
    public int getNumberOfParticipants() {
        return participants.size();
    }
    
    public void addParticipant(TUser u) {
        participants.add(u);
    }
    
    public void removeParticipant(TUser u) {
        participants.remove(u);
    }
    
    public void setLocation(Location loc) {
        this.loc = loc;
    }
    
    public Location getLocation() {
        return loc;
    }
    
    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    
    public boolean getIsPrivate() {
        return isPrivate;
    }

    public boolean isIsAccomplished() {
        return isAccomplished;
    }

    public void setIsAccomplished(boolean isAccomplished) {
        this.isAccomplished = isAccomplished;
    }
    
    public void setEventTimeRange(EventTimeRange range) {
        this.timerange = range;
    }
    
     public EventTimeRange getEventTimeRange() {
        return timerange;
    }
    /*
    public void setCategory(Category cat) {
        this.cat.add(cat);
    }
    
    public List<Category> getCategory() {
        return cat;
    }
    */
    public void setTimeCreated(Date time) {
        this.timeCreated = time;
    }
    
    public Date getTimeCreated() {
        return timeCreated;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj == null ) {
            return false;
        }
        if( obj.getClass().equals(this.getClass()) ) {
            try {
                Event other = (Event) obj;
                return other.hashCode() == this.hashCode();
            } catch(Exception e) {
                return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return eventTitle.hashCode() + eventDescription.hashCode();
    }

    /**
     * Default sorting is by title ascending.
     * 
     * TODO: Extend it to several sorting methods.
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Event o) {
        return eventTitle.compareTo(o.getTitle());
    }

    @Override
    public String toString() {
        return "Event { id=" + id + ", title=" + eventTitle +", desc=" + eventDescription + " } ";
    }

}
