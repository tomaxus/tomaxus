package edu.chl.group9.tomaxus.model.dbaccess;

import edu.chl.group9.tomaxus.model.BlogEvent;
import java.util.List;

/**
 *
 * @author peter
 */
public interface IBlogRegistry extends IAbstractRegistry<BlogEvent> {
    
    public List<BlogEvent> getBlogsForEvent(long id);
    
}
