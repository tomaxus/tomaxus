/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import java.util.List;

/**
 * Abstract interface for all registries.
 * 
 * @author Erik
 */
public interface IAbstractRegistry<T> {
    public void add(T t);
    public void remove(T t);
    public void remove(long id);
    public T update(T id);
    public T find(long id);
    public List<T> search(String t);
    public List<T> getAll();
    public long getCount();
}
