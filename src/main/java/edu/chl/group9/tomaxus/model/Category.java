/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Daniel
 */
@Entity
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    @OneToMany
    private List<Event> events;
    private String imageURL;
    public static final String DEFAULT_CATEGORY_IMAGE = "../images/profile/Sample_Profile_Image.png";

    public Category() {
    }

    public Category(String name, String description, List<Event> events) {
        this.name = name;
        this.description = description;
        if(events != null) {
            this.events = events;
        } else {
            this.events = new ArrayList<>();
        }
    }

    public Category(String name, String desc) {
        this.name = name;
        description = desc;
        this.events = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addEvent(Event ev) {
        events.add(ev);
    }

    public void removeEvent(Event ev) {
        events.remove(ev);
    }

    public void removeAll() {
        events.clear();
    }

    public List<Event> getEvents() {
        return events;
    }

    public boolean containsEvent(Event ev) {
        if (events.contains(ev)) {
            return true;
        } else {
            return false;
        }
    }

    public String getImageURL() {
        if (imageURL != null) {
            return imageURL;
        } else {
            return DEFAULT_CATEGORY_IMAGE;
        }
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Category) {
            Category other = (Category) obj;
            return hashCode() == other.hashCode();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        hash = 61 * hash + Objects.hashCode(this.name);
        hash = 61 * hash + Objects.hashCode(this.description);
        return hash;
    }
    
    public int getNumberOfEvents() {
        return events.size();
    }
}
