/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.model.dbaccess;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * Abstract data access object.
 *
 * All registry classes extends this class.
 *
 *
 * @author Erik
 */
public abstract class AbstractRegistryDAO<T> implements IAbstractRegistry<T> {

    protected EntityManagerFactory emf;
    protected final Class<T> clazz;

    public AbstractRegistryDAO(String puName, Class<T> clazz) {
        this.clazz = clazz;
        emf = Persistence.createEntityManagerFactory(puName);
    }

    @Override
    public void add(T t) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(t);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void remove(T t) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            T item = em.merge(t); //needed?
            em.remove(item);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void remove(long id) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            T t = em.getReference(clazz, id);
            em.remove(t);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public T update(T t) {
        EntityManager em = null;
        T obj = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            obj = em.merge(t);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
        return obj;
    }

    @Override
    public T find(long id) {
        EntityManager em = null;
        T ret = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();
            ret = em.find(clazz, id);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
        return ret;
    }

    @Override
    public List<T> getAll() {
        EntityManager em = null;
        List<T> found = new ArrayList<>();
        try {
            em = emf.createEntityManager();
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(clazz));
            Query q = em.createQuery(cq);

            found.addAll(q.getResultList());

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return found;
    }

    @Override
    public long getCount() {
        return getAll().size();
    }
}
