/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.test;

import edu.chl.group9.tomaxus.model.Category;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.Location;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.ICategoryRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IEventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Daniel
 */
public class CategoryTest {
    
    public CategoryTest(){
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public void testAll(){
        testAddCategory();
        testRemoveCategory();
        testGetEventsFromCategory();
    }
    

    public void testAddCategory(){
        ICategoryRegistry reg = Tomaxus.INSTANCE.getCategoryRegistry();
        IEventRegistry reg1 = Tomaxus.INSTANCE.getEventRegistry();
        ITUserRegistry reg2 = Tomaxus.INSTANCE.getUserRegistry();
        List<Event> events = new ArrayList<>();
        

        TUser gubbe1 = new TUser("Evert", "Evertsson", "Karlsson");
        TUser gubbe2 = new TUser("Sven", "Gunnar", "Gunnarson");
        TUser gubbe3 = new TUser("Clara", "Tiara", "Gunnarson");
        TUser gubbe4 = new TUser("Bertil", "fiskarn", "Jonsson");
        
        Event e1 = new Event("Löpning", "Ska kunna springa 10mil", 
                   new Location("Göteborg", "Sverige","Bergen 8"),
                   gubbe1, true);
                  
        
        Event e2 = new Event("Pussas", "Ska ha pussat en flicka", 
                  new Location("Stockholm", "Sverige","Bratzgatan 08"),
                  gubbe2, false);
        
        Event e3 = new Event("Fotspikning", "Spika mig i foten utan att få ont", 
                   new Location("Malmö", "Sverige","Hamnen 5"),
                   gubbe3, true);
        
        Event e4 = new Event("Fiska", "Fånga en haj med metspö", 
                  new Location("Kapstaden", "Sydafrika","Oceanstreet 51"),
                  gubbe4, true);
        
        events.add(e1);
        events.add(e2);
        events.add(e3);
        events.add(e4);
       
        Category cat = new Category("Resor", "Här ligger allt som handlar om resor",events );
        
        reg2.add(gubbe1);
        reg2.add(gubbe2);
        reg2.add(gubbe3);
        reg2.add(gubbe4);
        
        reg1.add(e1);
        reg1.add(e2);
        reg1.add(e3);
        reg1.add(e4);
        
        long sizeBefore = reg.getCount();
        reg.add(cat);
        long sizeAfter =  reg.getCount();
       
        
        reg.remove(cat);
        
        reg1.remove(e1);
        reg1.remove(e2);
        reg1.remove(e3);
        reg1.remove(e4);
        
        reg2.remove(gubbe1);
        reg2.remove(gubbe2);
        reg2.remove(gubbe3);
        reg2.remove(gubbe4);
        
        Assert.assertTrue(!(sizeBefore < 0));
        Assert.assertTrue(!(sizeBefore == sizeAfter));
        Assert.assertTrue(sizeAfter == sizeBefore + 1 );
    }
    

    public void testRemoveCategory(){
        ICategoryRegistry reg = Tomaxus.INSTANCE.getCategoryRegistry();
        IEventRegistry reg1 = Tomaxus.INSTANCE.getEventRegistry();
        ITUserRegistry reg2 = Tomaxus.INSTANCE.getUserRegistry();
        List<Event> events = new ArrayList<>();

        
        TUser gubbe1 = new TUser("Evert", "Evertsson", "Karlsson");
        TUser gubbe2 = new TUser("Sven", "Gunnar", "Gunnarson");

        
        Event e1 = new Event("Löpning", "Ska kunna springa 10mil", 
                   new Location("Göteborg", "Sverige","Bergen 8"),
                   gubbe1, true);
                  
        
        Event e2 = new Event("Pussas", "Ska ha pussat en flicka", 
                  new Location("Stockholm", "Sverige","Bratzgatan 08"),
                  gubbe2, false);
        
        events.add(e1);
        events.add(e2);
        
        Category cat = new Category("Resor", "Här ligger allt som handlar om resor",events );
        
        reg2.add(gubbe1);
        reg2.add(gubbe2);
        
        reg1.add(e1);
        reg1.add(e2);
        reg.add(cat);
        
        long sizeBefore = reg.getCount();
        reg.remove(cat);
        long sizeAfter =  reg.getCount();
       
              
        reg1.remove(e1);
        reg1.remove(e2);
        
        reg2.remove(gubbe1);
        reg2.remove(gubbe2);
        
        Assert.assertTrue(!(sizeBefore < 0));
        Assert.assertTrue(!(sizeBefore == sizeAfter));
        Assert.assertTrue(sizeAfter == sizeBefore - 1 );
    }
    

    public void testGetEventsFromCategory(){
        ICategoryRegistry reg = Tomaxus.INSTANCE.getCategoryRegistry();
        IEventRegistry reg1 = Tomaxus.INSTANCE.getEventRegistry();
        ITUserRegistry reg2 = Tomaxus.INSTANCE.getUserRegistry();
        List<Event> events = new ArrayList<>();        
        List<Event> foundEvents = new ArrayList<>();
        TUser gubbe1 = new TUser("Evert", "Evertsson", "Karlsson");
        TUser gubbe2 = new TUser("Sven", "Gunnar", "Gunnarson");

        
        Event e1 = new Event("Löpning", "Ska kunna springa 10mil", 
                   new Location("Göteborg", "Sverige","Bergen 8"),
                   gubbe1, true);
                  
        
        Event e2 = new Event("Pussas", "Ska ha pussat en flicka", 
                  new Location("Stockholm", "Sverige","Bratzgatan 08"),
                  gubbe2, false);
        
        events.add(e1);
        events.add(e2);
        
        Category cat = new Category("Resor", "Här ligger allt som handlar om resor",events );
        
        reg2.add(gubbe1);
        reg2.add(gubbe2);
        
        reg1.add(e1);
        reg1.add(e2);
        reg.add(cat);
        foundEvents = reg.getEventsFromCategory(cat.getId());
        
        
        reg.remove(cat);
        
        reg1.remove(e1);
        reg1.remove(e2);
        
        reg2.remove(gubbe1);
        reg2.remove(gubbe2);
        

        Assert.assertTrue(foundEvents.size() == 2);
        Assert.assertTrue(e1.equals(foundEvents.get(0)));
        Assert.assertTrue(e2.equals(foundEvents.get(1)));

   }
   
}
