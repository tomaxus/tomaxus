package edu.chl.group9.tomaxus.test;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.dbaccess.EventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.IEventRegistry;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * Test class for Event and EventRegistry.
 * 
 * @author Erik
 */
public class EventTest {
    
    public EventTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }    
    
    @Test
    public void testAdd() {
        IEventRegistry er = EventRegistry.newInstance("tomaxus");
        Event ev = new Event();
        long sizeBefore = er.getCount();
        ev.setDescription("Träffa fina damer.");
        ev.setTitle("Date.");
        er.add(ev);
        long sizeAfter = er.getCount();
        Assert.assertTrue(sizeBefore == (sizeAfter - 1) );
    }
    
    @Test
    public void testFind() {
        IEventRegistry er = Tomaxus.INSTANCE.getEventRegistry();
        Event ev = new Event();
        ev.setDescription("Test find desc");
        ev.setTitle("Test title.");
        er.add(ev);
        Event eventFromJPA = er.find(ev.getId());
        Assert.assertTrue(eventFromJPA != null);
    }
    
    @Test
    public void testRemove() {
        IEventRegistry er =  Tomaxus.INSTANCE.getEventRegistry();
        Event ev = new Event();
        long sizeBefore = er.getCount();
        ev.setDescription("Träffa fina damer.");
        ev.setTitle("Date.");
        er.add(ev);
        er.remove(ev.getId());
        long sizeAfter = er.getCount();
        Assert.assertTrue(sizeBefore == sizeAfter );
    }
    
    @Test
    public void testEquals() {
        Event ev1 = new Event();
        Event ev2 = new Event();
        ev1.setDescription("Test find desc");
        ev2.setDescription("Test find desc");
        ev1.setTitle("Test title.");
        ev2.setTitle("Test title.");
        Assert.assertTrue(ev1.equals(ev2));
    }
    
    //@Test
    public void testSearch() {
        IEventRegistry er =  Tomaxus.INSTANCE.getEventRegistry();
        List<Event> result = er.search("räk");
        Assert.assertTrue(result.size() == 1);
    }
    
    //@Test
    public void testGetParticipants() {
        IEventRegistry er =  Tomaxus.INSTANCE.getEventRegistry();
        ITUserRegistry ur =  Tomaxus.INSTANCE.getUserRegistry();
        TUser pelle = new TUser("Pelle", "Pelle", "Pellesson");
        TUser arne = new TUser("Arne", "Arne", "Arnesson");
        ur.add(arne);
        ur.add(pelle);
        Event t = new Event("Börja snusa", "VI ska börja snus", null, arne, true);
        //t.addParticipant(arne);
        t.addParticipant(pelle);
        er.add(t);        
        List<TUser> list = er.getUsersFromEvent(t.getId());
        Assert.assertEquals(t.getParticipants(), list);
    }
    
    //@Test
    public void testGetEventForUser(){
        IEventRegistry er =  Tomaxus.INSTANCE.getEventRegistry();
        ITUserRegistry ur =  Tomaxus.INSTANCE.getUserRegistry();
        TUser peter = new TUser("PeterTheCool", "Peter", "Petersson");
        ur.add(peter);
        Event t1 = new Event("Börja snusa", "VI ska börja snus", null, peter, true);
        //t1.addParticipant(peter);
        er.add(t1);   
        Event t2 = new Event("Bli tjock", "Trött på att vara smal...", null, peter, true);
        //t2.addParticipant(peter);
        er.add(t2);           
        Assert.assertTrue(er.getEventsFromUser(peter.getId()).size() == 2);
    }
    
}
