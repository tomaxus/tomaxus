/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.test;

import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.dbaccess.ITUserRegistry;
import edu.chl.group9.tomaxus.model.Tomaxus;
import edu.chl.group9.tomaxus.model.TUser;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author temp
 */
public class UserTest {
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddUser() {
        ITUserRegistry ureg = Tomaxus.INSTANCE.getUserRegistry();
        
        long sizeBefore = ureg.getCount();
        TUser u = new TUser("nisse", "nils","nilsson");
        
        Event e = new Event();
        e.setTitle("Räkna");
        e.setDescription("Jag ska lära mig räkna!");
        /*
        BlogEvent be = new BlogEvent("Jag har lärt mig 1+1!", true);
        
        e.addBlogEvent(be);
        */
        
        ureg.add(u);
        
        long sizeAfter = ureg.getCount();
        
        List<TUser> l = ureg.getAll();
        
        Assert.assertTrue(sizeBefore == sizeAfter - 1);
        
        TUser t = new TUser("bosse","bo","larsson");
        ureg.add(t);
        l = ureg.getAll();
        Assert.assertTrue(sizeBefore + 2 == l.size());
        
        u.setFname("lennart");
        
        ureg.update(u);
        
        ureg.remove(t);
    }
    
}
