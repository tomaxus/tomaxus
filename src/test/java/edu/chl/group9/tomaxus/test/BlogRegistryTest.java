/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.group9.tomaxus.test;

import edu.chl.group9.tomaxus.model.BlogEvent;
import edu.chl.group9.tomaxus.model.Event;
import edu.chl.group9.tomaxus.model.TUser;
import edu.chl.group9.tomaxus.model.Tomaxus;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class BlogRegistryTest {
    
    public BlogRegistryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    // Array index ouy of range: 0
    // Får det felet här //Erik
    //@Test
    public void testGetBlogsForEvent() {
        
        TUser u1 = new TUser("nisse", "nils", "nilsson");
        
        Event e1 = new Event("Hoppa", "hoppa högt", null, u1, true);
        
        Event e2 = new Event("Sjunga", "tre oktaver", null, u1, true);
        
        BlogEvent b1 = new BlogEvent("hoppa","Jag har hoppat 20 cm", true, e1,u1);
        BlogEvent b2 = new BlogEvent("hoppa","Jag har hoppat 50 cm!", true, e1,u1);
        BlogEvent b3 = new BlogEvent("sjung","Tre toner än så länge!", true, e2,u1);
        
        Tomaxus.INSTANCE.getUserRegistry().add(u1);
        
        Tomaxus.INSTANCE.getEventRegistry().add(e1);
        Tomaxus.INSTANCE.getEventRegistry().add(e2);
        
        Tomaxus.INSTANCE.getBlogRegistry().add(b1);
        Tomaxus.INSTANCE.getBlogRegistry().add(b2);
        Tomaxus.INSTANCE.getBlogRegistry().add(b3);
     
        List<BlogEvent> list = Tomaxus.INSTANCE.getBlogRegistry().getBlogsForEvent(e1.getId());
        
        Logger.getAnonymousLogger().log(Level.OFF, "bevents: " + list.get(0).getBlogContent());
        Logger.getAnonymousLogger().log(Level.OFF, "bevents: " + list.get(1).getBlogContent());
        Assert.assertTrue(list.size() == 2);
    }
}
